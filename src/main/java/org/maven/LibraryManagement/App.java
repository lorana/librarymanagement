package org.maven.LibraryManagement;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.maven.LibraryManagement.model.Client;

public class App {
	static SessionFactory sessionFactory;

	public static void main(String[] args) {

		sessionFactory = new Configuration().configure().buildSessionFactory();
		List<Client> clients = getAllClients();
		for (Client client : clients) {
			System.out.println(client);
		}
	}

	public static List<Client> getAllClients() {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		List<Client> clients = session.createQuery("from Client").list();

		for (Client client : clients) {
			System.out.println(client);
		}

		tx.commit();
		session.close();

		return clients;

	}
}
