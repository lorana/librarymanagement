package org.maven.LibraryManagement.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MembershipType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "memb_type")
	private static String membershipType;
	@Column(name = "price")
	private static double price;

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		MembershipType.id = id;
	}

	public static String getMembershipType() {
		return membershipType;
	}

	public static void setMembershipType(String membershipType) {
		MembershipType.membershipType = membershipType;
	}

	public static double getPrice() {
		return price;
	}

	public static void setPrice(double price) {
		MembershipType.price = price;
	}

}
