package org.maven.LibraryManagement.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class BorrowedBook {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "client_id")
	private static Client clientID;
	@Column(name = "book_id")
	private static Book bookId;
	@Column(name = "start_date")
	private static LocalDateTime startDate;
	@Column(name = "end_date")
	private static LocalDateTime endDate;

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		BorrowedBook.id = id;
	}

	public static Client getClientID() {
		return clientID;
	}

	public static void setClientID(Client clientID) {
		BorrowedBook.clientID = clientID;
	}

	public static Book getBookId() {
		return bookId;
	}

	public static void setBookId(Book bookId) {
		BorrowedBook.bookId = bookId;
	}

	public static LocalDateTime getStartDate() {
		return startDate;
	}

	public static void setStartDate(LocalDateTime startDate) {
		BorrowedBook.startDate = startDate;
	}

	public static LocalDateTime getEndDate() {
		return endDate;
	}

	public static void setEndDate(LocalDateTime endDate) {
		BorrowedBook.endDate = endDate;
	}

}
