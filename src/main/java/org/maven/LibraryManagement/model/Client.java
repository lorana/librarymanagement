package org.maven.LibraryManagement.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Embeddable
@Table(name = "clients")
public class Client {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "first_name")
	private static String firstName;
	@Column(name = "last_name")
	private static String lastName;
	@Column(name = "phone")
	private static String phone;
	@Column(name = "email")
	private static String email;

	@OneToMany(mappedBy = "client")

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		Client.id = id;
	}

	public static String getFirstName() {
		return firstName;
	}

	public static void setFirstName(String firstName) {
		Client.firstName = firstName;
	}

	public static String getLastName() {
		return lastName;
	}

	public static void setLastName(String lastName) {
		Client.lastName = lastName;
	}

	public static String getPhone() {
		return phone;
	}

	public static void setPhone(String phone) {
		Client.phone = phone;
	}

	public static String getEmail() {
		return email;
	}

	public static void setEmail(String email) {
		Client.email = email;
	}

}
