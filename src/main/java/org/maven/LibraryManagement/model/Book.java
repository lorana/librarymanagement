package org.maven.LibraryManagement.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Embeddable
@Table(name = "Books")
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "title")
	private static String title;
	@Column(name = "author")
	private static String author;
	@Column(name = "borrow_id")
	private static int borrowId;

	@OneToMany(mappedBy = "book")

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		Book.id = id;
	}

	public static String getBook() {
		return title;
	}

	public static void setBook(String title) {
		Book.title = title;
	}

	public static String getAuthor() {
		return author;
	}

	public static void setAuthor(String author) {
		Book.author = author;
	}

	public static int getBorrow_id() {
		return borrowId;
	}

	public static void setBorrow_id(int borrowId) {
		Book.borrowId = borrowId;
	}

}
