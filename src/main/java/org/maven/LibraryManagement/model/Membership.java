package org.maven.LibraryManagement.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Membership {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "client_id")
	private static Client clientId;
	@Column(name = "memb_type_id")
	private static MembershipType membId;
	@Column(name = "start_date")
	private static LocalDateTime startDate;
	@Column(name = "end_date")
	private static LocalDateTime endDate;

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		Membership.id = id;
	}

	public static Client getClientId() {
		return clientId;
	}

	public static void setClientId(Client clientId) {
		Membership.clientId = clientId;
	}

	public static MembershipType getMembId() {
		return membId;
	}

	public static void setMembId(MembershipType membId) {
		Membership.membId = membId;
	}

	public static LocalDateTime getStartDate() {
		return startDate;
	}

	public static void setStartDate(LocalDateTime startDate) {
		Membership.startDate = startDate;
	}

	public static LocalDateTime getEndDate() {
		return endDate;
	}

	public static void setEndDate(LocalDateTime endDate) {
		Membership.endDate = endDate;
	}

}
