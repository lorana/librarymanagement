package org.maven.LibraryManagement.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class BorrowPeriod {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "timeframe")
	private static String timeFrame;

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		BorrowPeriod.id = id;
	}

	public static String getTimeFrame() {
		return timeFrame;
	}

	public static void setTimeFrame(String timeFrame) {
		BorrowPeriod.timeFrame = timeFrame;
	}

}
