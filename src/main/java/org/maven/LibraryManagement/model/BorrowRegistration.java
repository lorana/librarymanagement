package org.maven.LibraryManagement.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class BorrowRegistration {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private static int id;
	@Column(name = "client_id")
	private static Client clientId;
	@Column(name = "book_id")
	private static Book bookID;
	@Column(name = "start_date")
	private static LocalDateTime startDate;
	@Column(name = "end_date")
	private static LocalDateTime endDate;
	@Column(name = "return_date")
	private static LocalDateTime returnDate;

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		BorrowRegistration.id = id;
	}

	public static Client getClientId() {
		return clientId;
	}

	public static void setClientId(Client clientId) {
		BorrowRegistration.clientId = clientId;
	}

	public static Book getBookID() {
		return bookID;
	}

	public static void setBookID(Book bookID) {
		BorrowRegistration.bookID = bookID;
	}

	public static LocalDateTime getStartDate() {
		return startDate;
	}

	public static void setStartDate(LocalDateTime startDate) {
		BorrowRegistration.startDate = startDate;
	}

	public static LocalDateTime getEndDate() {
		return endDate;
	}

	public static void setEndDate(LocalDateTime endDate) {
		BorrowRegistration.endDate = endDate;
	}

	public static LocalDateTime getReturnDate() {
		return returnDate;
	}

	public static void setReturnDate(LocalDateTime returnDate) {
		BorrowRegistration.returnDate = returnDate;
	}

}
